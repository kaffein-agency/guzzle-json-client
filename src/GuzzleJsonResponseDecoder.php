<?php namespace KaffeinAgency\GuzzleJsonClient;

use GuzzleHttp\Psr7\Response;
use JsonException;

class GuzzleJsonResponseDecoder {
	private Response $response;

	public function __construct(
		Response $response
	) {
		$this->response = $response;
	}

	public function decode() {
		try {
			return json_decode( $this->response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR );
		} catch ( JsonException $e ) {
			return (new ArrayableException($e))->toArrray();
		}
	}
}
