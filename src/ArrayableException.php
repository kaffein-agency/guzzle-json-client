<?php namespace KaffeinAgency\GuzzleJsonClient;

class ArrayableException extends \Exception {
	public function toArrray() {
		return [
			"error" => $this->getMessage(),
			"trace" => $this->getTrace()
		];
	}
}
