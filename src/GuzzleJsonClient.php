<?php namespace KaffeinAgency\GuzzleJsonClient;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use JsonException;

class GuzzleJsonClient {
	private Client $client;

	private static array $requestsResultsByHash = [];

    public function __construct( $params = [] ) {
		$this->client = new Client( array_merge_recursive( [
			"defaults" => [
				"headers" => [
					'Content-Type' => 'application/json',
					'Accept'       => 'application/json',
				]
			]
		], $params ) );
	}

	public function request( ...$requestParams ) {
		$requestHash = md5(serialize($requestParams));

		if(isset(self::$requestsResultsByHash[$requestHash])) {
			return self::$requestsResultsByHash[$requestHash];
		}

		try {
			$response = $this->client->request( ...$requestParams );

			$decodedResponse = self::makeJsonResponseDecoder( $response )->decode();

			self::$requestsResultsByHash[$requestHash] = $decodedResponse;

			return $decodedResponse;
		} catch ( RequestException $e ) {
            $response = $e->getResponse();
            if(!$response) {
                return null;
            }
			return self::makeJsonResponseDecoder( $response )->decode();
		} catch ( GuzzleException $e ) {
			return (new ArrayableException($e))->toArrray();
		}
	}

	public function output($data) {
		header( 'Content-Type: application/json' );
		try {
			echo json_encode( $data, JSON_THROW_ON_ERROR );
		} catch ( JsonException $e ) {
			echo json_encode( (new ArrayableException($e))->toArrray() );
		}
	}

	private static function makeJsonResponseDecoder( Response $response ): GuzzleJsonResponseDecoder {
		return new GuzzleJsonResponseDecoder($response);
	}
}
